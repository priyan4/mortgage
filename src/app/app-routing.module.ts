import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './components/auth/login/login.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { HomeComponent } from './components/home/home.component';
import { TemplateDocComponent } from './components/template-doc/template-doc.component';
import { DocumentCategoryComponent } from './components/document-category/document-category.component';
import { DocumentTypeComponent } from './components/document-type/document-type.component';
import { QuickpackComponent } from './components/quickpack/quickpack.component';



const routes: Routes = [
  { path: '', component: LoginComponent},
  {path: 'dashboard', component: DashboardComponent  },
  {path: 'home', component: HomeComponent  },
  {path: 'category', component: DocumentCategoryComponent  },
  {path: 'template_doc', component: TemplateDocComponent  },
  {path: 'type', component: DocumentTypeComponent  },
  {path: 'quickpack', component: QuickpackComponent  },



];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

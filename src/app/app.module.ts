import { BrowserModule } from '@angular/platform-browser';
import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ReactiveFormsModule} from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
// import { BsDropdownModule } from 'ngx-bootstrap';

import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import {MatIconModule} from '@angular/material/icon';
import {MatListModule} from '@angular/material/list';
import {MatMenuModule} from '@angular/material/menu';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatTreeModule} from '@angular/material/tree';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';

import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import {MatCardModule, MatFormFieldModule, MatInputModule, MatPaginatorModule, MatSortModule, MatTableModule, MatSelectModule, MatTooltipModule, MatDialogModule, MatRadioModule, MatButtonModule, MatCheckboxModule} from '@angular/material';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { DashboardComponent, DialogOverviewExampleDialog } from './components/dashboard/dashboard.component';
import { LoginComponent } from './components/auth/login/login.component';
import { HomeComponent } from './components/home/home.component';
import { TemplateDocComponent, AddNewDialog } from './components/template-doc/template-doc.component';
import { DocumentCategoryComponent } from './components/document-category/document-category.component';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { DocumentTypeComponent } from './components/document-type/document-type.component';
import { QuickpackComponent, AddQuickPackDialog, EditQuickPackDialog } from './components/quickpack/quickpack.component';


const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};
const routes: Routes = [];

@NgModule({
  declarations: [
    AppComponent,
    SidebarComponent,
    DashboardComponent,
    DialogOverviewExampleDialog,
    LoginComponent,
    HomeComponent,
    TemplateDocComponent,
    DocumentCategoryComponent,
    AddNewDialog,
    DocumentTypeComponent,
    QuickpackComponent,
    AddQuickPackDialog,
    EditQuickPackDialog,
  ],

  imports: [
    BrowserModule,
    AppRoutingModule,
    // BsDropdownModule.forRoot(),
    PerfectScrollbarModule,
    MatIconModule,
    MatMenuModule,
    MatListModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatTooltipModule,
    AngularEditorModule ,
    MatTreeModule,
    MatSidenavModule,
    MatSlideToggleModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatCardModule,
    MatFormFieldModule,
    MatSelectModule,
    MatButtonModule,
    FormsModule,
    MatDialogModule,
    MatRadioModule,
    ReactiveFormsModule,
    MatInputModule,
    HttpClientModule,
    MatInputModule,
    RouterModule,
    MatCheckboxModule,

  ],
  entryComponents:[
    DialogOverviewExampleDialog,
    AddNewDialog,
    AddQuickPackDialog,
    EditQuickPackDialog,
  ],
  providers: [ {
    provide: PERFECT_SCROLLBAR_CONFIG,
    useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
  }],
  bootstrap: [AppComponent],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ],

})
export class AppModule { }

import { Component } from '@angular/core';

import { environment } from 'src/environments/environment.prod';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { SidebarService } from './components/sidebar/sidebar.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'mortgage';
  environment: { production: boolean; };
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );
    route: any;

  constructor(public sidebarservice: SidebarService,private location:Location,private breakpointObserver: BreakpointObserver,public router:Router) { 
    this.environment=environment;
    router.events.subscribe((val) => {
      console.log(this.route = location.path())
    });

  }
  toggleSidebar() {
    // this.sidebarservice.setSidebarState(!this.sidebarservice.getSidebarState());
    // document.getElementById("autoSize").style.width = "75%";
    document.getElementById("sidebar").style.width ="260px";
    document.getElementById("autoSizeMain").classList.remove('p_30');
    document.getElementById("autoSizeMain").classList.add('p_5');
  }


  toggleBackgroundImage() {
    this.sidebarservice.hasBackgroundImage = !this.sidebarservice.hasBackgroundImage;

  }
  getSideBarState() {

    return this.sidebarservice.getSidebarState();

  }
  ngOnInit(){
  }
  hideSidebar() {
    // this.sidebarservice.setSidebarState(true);
    document.getElementById("sidebar").style.width ="86px";

    document.getElementById("autoSize").style.width = "100%";
    document.getElementById("autoSizeMain").classList.remove('p_5');
    document.getElementById("autoSizeMain").classList.add('p_30');

  }
  
  logout(){
    localStorage.clear();
    this.router.navigate(['/']);

  }
}

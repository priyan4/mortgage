import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class DatabaseService {

  public headers = new HttpHeaders({
    'Content-Type':  'application/json',
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Methods': 'GET,PUT,POST,DELETE,PATCH,OPTIONS',
    'No-Auth': 'True'
  }); 
  data: any;
  
  constructor( private http: HttpClient, private router: Router) { }

  userLogin(username, password,type,remember_me) {
    let headers = new HttpHeaders({
    'Content-Type': 'application/json',
    });
    let options = { headers: headers };
    if(type==1){
      this.data= {
        "email":username,
        "password":password
        }
    }
    if(type==2){
      this.data= {
        "mobile":username,
        "password":password
        }
    }
  
    return this.http.post(`${environment.apiUrl}api/auth/login`, this.data, options);
  }

  userDetail(){
    var access_token=localStorage.getItem("auth_token");
    let headers = new HttpHeaders({
      'Content-Type': 'application/json;charset=utf-8',
      Authorization:  'Bearer ' +''+access_token
       });
      let options = { headers: headers };
     
      return this.http.get(`${environment.apiUrl}api/auth/user`,options);
  }

}

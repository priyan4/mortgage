import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators} from '@angular/forms';
import { DatabaseService } from 'src/app/services/database.service';
import { Router } from '@angular/router';
import { first } from 'rxjs/operators';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginUser                   : FormGroup;
  emailValidation             : any    ='';
  error                       : boolean=false;

  constructor(public database:DatabaseService,public router:Router) { }

  ngOnInit(): void {
    this.createform();
  }

  createform(){
    this.loginUser=new FormGroup({
     email:new FormControl('',[Validators.required]),
     password:new FormControl('',[Validators.required,Validators.minLength(6)])
      
    });
  }

 
  keytab(event, el){
    console.log(event, el)
    let element = el; // get the sibling element

    if(element == null)  // check if its null
      {

        return;
      }
    else
        {element.focus();
        }  // focus if not null
}

  login(){
    this.error=false;
    function ValidateEmail(mail) 
    {
     if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail))
      {
        var data=
        {
          type:1,
          
        }
        
        return (data)
      }
      else if(/^[6-9]\d{9}$/.test(mail)){
        var data=
        {
          type:2,
          
        }
        
        return (data)
      }
       
        return (false)
    }


    this.emailValidation=ValidateEmail(this.loginUser.value.email);
    console.log(this.emailValidation)
    if(this.emailValidation!=false){
      this.database.userLogin(this.loginUser.value.email,this.loginUser.value.password,this.emailValidation.type,true).pipe(first()).subscribe((res:any)=>{
     
        console.log(res)
        localStorage.setItem("auth_token",res.user.access_token);

       if(res.user.is_verified==1){
         localStorage.setItem("account_type",res.user.account_type);
         this.database.userDetail().pipe(first()).subscribe((res:any)=>{
           console.log(res);
           localStorage.setItem("UserId",res.id);
         })
           this.router.navigate(['/dashboard']);
       }
       else{
        Swal.fire({
          icon: 'warning',
          title: 'Oops...',
          text: "Your number isn't verified .please verify your number before you login",
          showCancelButton: false,
          confirmButtonColor: '#007bff',
         
          confirmButtonText: 'Ok'
        }).then((result) => {
          if (result.value) {
            localStorage.setItem("mobileOtp",res.user.mobile);
          }
        })
      
       }
     
      },err=>{
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: "Incorrect Email/mobile number",
          showClass: {
            popup: 'animated fadeInDown faster'
          },
          hideClass: {
            popup: 'animated fadeOutUp faster'
          }
        })
      });
    }
    else{
      this.error=true;
    }
  

   

  

  }
  register(){
    this.router.navigate(['/register']);
  }

 

}

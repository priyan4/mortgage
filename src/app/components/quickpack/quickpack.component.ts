import { Component, OnInit, ViewChild, Optional, Inject } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort, MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { AngularEditorConfig } from '@kolkov/angular-editor/lib/config';

export interface DialogData {
  data: any;
}
@Component({
  selector: 'app-quickpack',
  templateUrl: './quickpack.component.html',
  styleUrls: ['./quickpack.component.scss']
})
export class QuickpackComponent implements OnInit {
  displayedColumns: string[] = ['name','action'];
  dataSource = new MatTableDataSource<IPLData>(active);
  @ViewChild(MatPaginator, {static:true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static:true}) sort: MatSort;
  loan_filter:any='1';
  show_error:boolean=false;
  constructor(public dialog:MatDialog) { }
  ngOnInit() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
}

  addNew(){
    const dialogRef = this.dialog.open(AddQuickPackDialog, {
      maxHeight: '90vh',
      data: ''
    });
    dialogRef.afterClosed().subscribe(result => {
      this.dataSource = new MatTableDataSource<IPLData>(active);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    });
  }

  edit_loan(data){
    const dialogRef = this.dialog.open(EditQuickPackDialog, {
      maxHeight: '90vh',
      data: data
    });
    dialogRef.afterClosed().subscribe(result => {
      this.dataSource = new MatTableDataSource<IPLData>(active);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    });
  }

}



export interface IPLData {
  name:string;
  description:string;
  type:string;
  category:string;
  id:number;
  checked:boolean;
}


const active:IPLData[]=[
  {id:1,name:"1003-Loan Application",description:"Please complete all fields",type:"TenOThree",category:"Document Requested",checked:false},
  {id:1,name:"1003-Loan Application",description:"Please complete all fields",type:"Regular",category:"Document Requested",checked:false},
  {id:1,name:"1003-Loan Application",description:"Please complete all fields",type:"Regular",category:"Document Requested",checked:false},
  {id:1,name:"1003-Loan Application",description:"Please complete all fields",type:"Regular",category:"Document Requested",checked:false},
  {id:1,name:"1003-Loan Application",description:"Please complete all fields",type:"Regular",category:"Document Requested",checked:false}

];


@Component({
  selector: 'dialog-overview-example-dialog',
  templateUrl: 'addquickpack.component.html',
  styleUrls: ['./quickpack.component.scss']
})
export class AddQuickPackDialog {
  details: DialogData;
  selectall:false;
  displayedColumns: string[] = ['checked','name','category'];
  dataSource = new MatTableDataSource<IPLData>(active);
  @ViewChild(MatPaginator, {static:true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static:true}) sort: MatSort;
   constructor(
      @Optional()   public dialogRef: MatDialogRef<AddQuickPackDialog>,
      @Optional()  @Inject(MAT_DIALOG_DATA) public data: DialogData) {
 
this.details=data;

  
    }

    ngOnInit(){
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    }
      applyFilter(filterValue: string) {
        this.dataSource.filter = filterValue.trim().toLowerCase();
    }

    checkUncheckAll() {
      for (var i = 0; i < active.length; i++) {
        active[i].checked = this.selectall;
      }
      // this.getCheckedItemList();
      this.dataSource = new MatTableDataSource<IPLData>(active);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    }
   
    // getCheckedItemList(){
    //   this.checkedList = [];
    //   for (var i = 0; i < this.checklist.length; i++) {
    //     if(this.checklist[i].isSelected)
    //     this.checkedList.push(this.checklist[i]);
    //   }
    //   this.checkedList = JSON.stringify(this.checkedList);
    // }

  onNoClick(): void {
   
  }

  addquickpack(){
    var selected=[];
    for(let i=0;i<active.length;i++){
      if(active[i].checked==true){
       selected.push(active[i]);
      }
    }
    console.log(selected);
  }


}



@Component({
  selector: 'dialog-overview-example-dialog',
  templateUrl: 'editquickpack.component.html',
  styleUrls: ['./quickpack.component.scss']
})
export class EditQuickPackDialog {
  details: DialogData;
  selectall:false;
  name:string;
  displayedColumns: string[] = ['checked','name','category'];
  dataSource = new MatTableDataSource<IPLData>(active);
  @ViewChild(MatPaginator, {static:true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static:true}) sort: MatSort;
   constructor(
      @Optional()   public dialogRef: MatDialogRef<EditQuickPackDialog>,
      @Optional()  @Inject(MAT_DIALOG_DATA) public data: DialogData) {
 console.log(data)
this.details=data;
// console.log(data.name)

  
    }

    ngOnInit(){
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    }
      applyFilter(filterValue: string) {
        this.dataSource.filter = filterValue.trim().toLowerCase();
    }

    checkUncheckAll() {
      for (var i = 0; i < active.length; i++) {
        active[i].checked = this.selectall;
      }
      // this.getCheckedItemList();
      this.dataSource = new MatTableDataSource<IPLData>(active);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    }
   
    // getCheckedItemList(){
    //   this.checkedList = [];
    //   for (var i = 0; i < this.checklist.length; i++) {
    //     if(this.checklist[i].isSelected)
    //     this.checkedList.push(this.checklist[i]);
    //   }
    //   this.checkedList = JSON.stringify(this.checkedList);
    // }

  onNoClick(): void {
   
  }

  addquickpack(){
    var selected=[];
    for(let i=0;i<active.length;i++){
      if(active[i].checked==true){
       selected.push(active[i]);
      }
    }
    console.log(selected);
  }


}






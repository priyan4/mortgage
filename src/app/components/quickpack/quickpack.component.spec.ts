import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuickpackComponent } from './quickpack.component';

describe('QuickpackComponent', () => {
  let component: QuickpackComponent;
  let fixture: ComponentFixture<QuickpackComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuickpackComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuickpackComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { SidebarService } from './sidebar.service';
// import { MenusService } from './menus.service';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
  animations: [
    trigger('slide', [
      state('up', style({ height: 0 })),
      state('down', style({ height: '*' })),
      transition('up <=> down', animate(200))
    ])
  ]
})
export class SidebarComponent implements OnInit {
  menus = [];
  activeDrop:boolean=false;
  currentarrow: any;
  route: any;

  constructor(public sidebarservice: SidebarService,private location:Location,public router:Router) {
    this.menus = sidebarservice.getMenuList();

    router.events.subscribe((val) => {
      console.log(this.route = location.path())
    });
   }
  ngOnInit() {
  }

  getSideBarState() {
    return this.sidebarservice.getSidebarState();
  }

  toggle(currentMenu) {
    if (currentMenu.type === 'dropdown') {
      this.menus.forEach(element => {
        if (element === currentMenu) {
          currentMenu.active = !currentMenu.active;
        } else {
          element.active = false;
        }
      });
    }
  }

  getState(currentMenu) {

    if (currentMenu.active) {
      return 'down';
    } else {
      return 'up';
    }

  }

  hasBackgroundImage() {
    return this.sidebarservice.hasBackgroundImage;
  }
  toggleSidebar() {
    document.getElementById("sidebar").style.width ="260px";
    document.getElementById("autoSize").style.width ="75%";

  
    for(var i=0; i<this.menus.length; i++ ){

        this.menus[i].class="show";
    }
    this.activeDrop=true;

  }

  closeSidebar() {
    document.getElementById("sidebar").style.width ="86px";
    document.getElementById("autoSize").style.width = "100%";
    // document.getElementById("autoSizeMain").classList.remove('p_5');
    // document.getElementById("autoSizeMain").classList.add('p_30');
    for(var i=0; i<=this.menus.length; i++ ){
      this.menus[i].class="hid";
      this.activeDrop=false;
      }
    }

}

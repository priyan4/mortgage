import { Injectable } from '@angular/core';
import * as $ from "jquery";
@Injectable({
  providedIn: 'root'
})
export class SidebarService {
  toggled = false;
  _hasBackgroundImage = false;
  menus = [

    {
      title: 'Home',
      icon: 'fa fa-tachometer-alt',

      class: 'hid',
      type: 'dropdown',
      // badge: {
      //   text: 'New ',
      //   class: 'badge-warning'
      // },
      submenus: [
        {
          title: 'Dashboard 1',
          link:'/dashboard',

          // badge: {
          //   text: 'Pro ',
          //   class: 'badge-success'
          // }
        },
        {
          title: 'Dashboard 2',
          link:'/category',
        },
        {
          title: 'Dashboard 3',
          link:'/category',
        }
      ]
    },
    {
      title: 'Documents',
      icon: 'fa fa-tachometer-alt',

      class: 'hid',
      type: 'dropdown',
      // badge: {
      //   text: 'New ',
      //   class: 'badge-warning'
      // },
      submenus: [
        {
          title: 'Category',
          link:'/category',

          // badge: {
          //   text: 'Pro ',
          //   class: 'badge-success'
          // }
        },
        {
          title: 'Type',
          link:'/type',
        },
        {
          title: 'Template',
          link:'/template_doc',
        }
      ]
    },
    {
      title: 'Prospects',
      icon: 'fa fa-shopping-cart',
      class: 'hid',
      type: 'dropdown',
      badge: {
        text: '3',
        class: 'badge-danger'
      },
      submenus: [
        {
          title: ' S-Sample2',
        },
        {
          title: ' S-Sample3'
        },
      
      ]
    },
    {
      title: 'Apps',
      icon: 'far fa-gem',
      class: 'hid',
      type: 'dropdown',
      submenus: [
        {
          title: ' S-Sample1',
        },
        {
          title: ' S-Sample1'
        },
        {
          title: 'Forms'
        }
      ]
    },
    {
      title: 'Settings',
      icon: 'fa fa-chart-line',
      class: 'hid',
      type: 'dropdown',
      submenus: [
        {
          title: ' S-Sample1',
        },
        {
          title: ' S-Sample1'
        },
        {
          title: ' S-Sample1'
        },
       
      ]
    },
    {
      title: 'Users',
      icon: 'fa fa-globe',
      class: 'hid',
      type: 'dropdown',
      submenus: [
        {
          title: ' S-Sample1',
        },
        {
          title: ' S-Sample1'
        }
      ]
    },

    {
      title: 'Info',
      icon: 'fa fa-globe',
      class: 'hid',
      type: 'dropdown',
      submenus: [
        {
          title: ' S-Sample1',
        },
        {
          title: ' S-Sample1'
        }
      ]
    },

  ];
  constructor() {
    $("#target").on({
      hover: function(){
        alert("sss")
      },  
      click: function(){
          //do on mouse click
      }  
  });
   }

  

  toggle() {
    alert("hit")
    this.toggled = ! this.toggled;
  }

  getSidebarState() {
    return this.toggled;
  }

  setSidebarState(state: boolean) {
    this.toggled = state;
  }

  getMenuList() {
    return this.menus;
  }

  get hasBackgroundImage() {
    return this._hasBackgroundImage;
  }

  set hasBackgroundImage(hasBackgroundImage) {
    this._hasBackgroundImage = hasBackgroundImage;
  }
}

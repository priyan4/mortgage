import { Component, OnInit, ViewChild, Inject, Optional } from '@angular/core';
import {MatPaginator, MatSort, MatTableDataSource, MatDialogRef, MAT_DIALOG_DATA, MatDialog} from '@angular/material';
export interface DialogData {
  data: any;
 
}

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  displayedColumns: string[] = ['flow', 'borrower','name','owner','status','action'];
    dataSource = new MatTableDataSource<IPLData>(active);
    @ViewChild(MatPaginator, {static:true}) paginator: MatPaginator;
    @ViewChild(MatSort, {static:true}) sort: MatSort;
    loan_filter:any='1';
    show_error:boolean=false;
    constructor(public dialog:MatDialog){
        
    }

    ngOnInit() {
        if(active.length==0){
            this.show_error=true;
        }
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
        console.log(active)
    }

    applyFilter(filterValue: string) {
        this.dataSource.filter = filterValue.trim().toLowerCase();
    }
    loanFilter(event,data){
        
        console.log(event)
        console.log(data)
        if(data==1){
            this.dataSource=new MatTableDataSource<IPLData>(active);
            if(active.length==0){
                this.show_error=true;
            }
        }
        if(data==2){
            this.dataSource=new MatTableDataSource<IPLData>(pending);
            if(pending.length==0){
                this.show_error=true;
            }
        }
        if(data==3){
            this.dataSource=new MatTableDataSource<IPLData>(archive);
            if(archive.length==0){
                this.show_error=true;
            }
        }

        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
  
    }

    delete_Loan(id){
        if(this.loan_filter=="1"){
            var index = active.findIndex(x => x.id === id);
            active.splice(index,1);
            this.dataSource=new MatTableDataSource(active);
            this.dataSource.sort = this.sort;
            this.dataSource.paginator = this.paginator;
            console.log(active)
            if(active.length==0){
                this.show_error=true;
            }
        }
        if(this.loan_filter=="2"){
            var index = pending.findIndex(x => x.id === id);
            pending.splice(index,1);
            this.dataSource=new MatTableDataSource(pending);
            this.dataSource.sort = this.sort;
            this.dataSource.paginator = this.paginator;
            console.log(pending)
            if(pending.length==0){
                this.show_error=true;
            }
        }
        if(this.loan_filter=="3"){
            var index = archive.findIndex(x => x.id === id);
            archive.splice(index,1);
            this.dataSource=new MatTableDataSource(archive);
            this.dataSource.sort = this.sort;
            this.dataSource.paginator = this.paginator;
            console.log(archive)
            if(archive.length==0){
                this.show_error=true;
            }
        }
      

    }

    archive_loan(id){
        if(this.loan_filter=="1"){
            var index = active.findIndex(x => x.id === id);
            archive.push(active[index]);
            active.splice(index,1);
            this.dataSource=new MatTableDataSource(active);
            
            this.dataSource.sort = this.sort;
            this.dataSource.paginator = this.paginator;
            console.log(active)
            if(active.length==0){
                this.show_error=true;
            }
        }
        if(this.loan_filter=="2"){
            var index = pending.findIndex(x => x.id === id);
            archive.push(pending[index]);
            pending.splice(index,1);
            if(pending.length==0){
                this.show_error=true;
            }
            this.dataSource=new MatTableDataSource(pending);
            this.dataSource.sort = this.sort;
            this.dataSource.paginator = this.paginator;
            console.log(pending)
        }
       
      

    }

    remove_archive(id,status){
        if(status==1){
            var index = archive.findIndex(x => x.id === id);
            active.push(archive[index]);
            archive.splice(index,1);
            this.dataSource=new MatTableDataSource(archive);
            this.dataSource.sort = this.sort;
            this.dataSource.paginator = this.paginator;
            if(active.length==0){
                this.show_error=true;
            }
            if(archive.length==0){
                this.show_error=true;
            }
        }
        if(status==2){
            var index = archive.findIndex(x => x.id === id);
            pending.push(archive[index]);
            archive.splice(index,1);
            this.dataSource=new MatTableDataSource(archive);
            this.dataSource.sort = this.sort;
            this.dataSource.paginator = this.paginator;
            if(pending.length==0){
                this.show_error=true;
            }
            if(archive.length==0){
                this.show_error=true;
            }
        }


    }

    edit_loan(data){
        const dialogRef = this.dialog.open(DialogOverviewExampleDialog, {
         
            data: data
          });
      
          dialogRef.afterClosed().subscribe(result => {
            console.log('The dialog was closed');
           console.log(result)
           if(this.loan_filter=="1"){
            var index = active.findIndex(x => x.id === result.data.id);
            active[index]=result.data;
            this.dataSource=new MatTableDataSource(active);
            this.dataSource.sort = this.sort;
            this.dataSource.paginator = this.paginator;
            console.log(active)
        }
        if(this.loan_filter=="2"){
            var index = pending.findIndex(x => x.id ===  result.data.id);
            pending[index]=result.data;
            this.dataSource=new MatTableDataSource(pending);
            this.dataSource.sort = this.sort;
            this.dataSource.paginator = this.paginator;
            console.log(pending)
        }
        if(this.loan_filter=="3"){
            var index = archive.findIndex(x => x.id ===  result.data.id);
            archive[index]=result.data;
            this.dataSource=new MatTableDataSource(archive);
            this.dataSource.sort = this.sort;
            this.dataSource.paginator = this.paginator;
            console.log(archive)
        }
      
          });

    }
    
}

export interface IPLData {
    count1:number,
    count2:number,
    count3:number,
    borrower:string;
    name: string;
    owner: string;
    status:string;
    loan_status:number;
    id:number;
}


const active:IPLData[]=[
{id:1,count1:10,loan_status:1,count2:0,count3:2,borrower:"Ken Bunch", name: 'Ken Bunch',owner: 'Seth Taylor', status:"10 owned"},
{id:2,count1:5,loan_status:1,count2:0,count3:2,borrower:"Aaban", name: 'Ken 1',owner: 'Seth Taylor', status:"1 owned"},
{id:3,count1:7,loan_status:1,count2:0,count3:2,borrower:"Aabharan", name: 'Ken 2',owner: 'Seth Taylor', status:"2 owned"},
{id:4,count1:2,loan_status:1,count2:0,count3:2,borrower:"Faakhir", name: 'Ken 3',owner: 'Seth Taylor', status:"3 owned"},
{id:5,count1:9,loan_status:1,count2:0,count3:2,borrower:"Jaagriti", name: 'Kumar',owner: 'Seth Taylor', status:"4 owned"},
{id:6,count1:5,loan_status:1,count2:0,count3:2,borrower:"Jaakkina", name: 'Kumar',owner: 'Seth Taylor', status:"7 owned"},
{id:7,count1:3,loan_status:1,count2:0,count3:2,borrower:"Obaidullah", name: 'Kumar',owner: 'Seth Taylor', status:"8 owned"},
{id:8,count1:5,loan_status:1,count2:0,count3:2,borrower:"Sandeep Kumar", name: 'Kumar',owner: 'Seth Taylor', status:"9 owned"},
];

const pending:IPLData[]=[
{id:9,count1:8,loan_status:2,count2:0,count3:2,borrower:"Saachee", name: 'Kumar-Refinance',owner: 'Seth Taylor', status:"1 owned"},
{id:10,count1:6,loan_status:2,count2:0,count3:2,borrower:"Saadaat", name: 'Kumar-Refinance',owner: 'Seth Taylor', status:"5 owned"},
{id:11,count1:9,loan_status:2,count2:0,count3:2,borrower:"Wacian", name: 'Kumar-Refinance',owner: 'Seth Taylor', status:"4 owned"},
{id:12,count1:3,loan_status:2,count2:0,count3:2,borrower:"Taanish", name: 'Kumar-Refinance',owner: 'Seth Taylor', status:"7 owned"},
{id:13,count1:4,loan_status:2,count2:0,count3:2,borrower:"Eadelmarr", name: 'Zach Juneau',owner: 'Seth Taylor', status:"8 owned"},
{id:14,count1:11,loan_status:2,count2:0,count3:2,borrower:"Eadger", name: 'Zach Juneau',owner: 'Seth Taylor', status:"6 owned"},
{id:15,count1:14,loan_status:2,count2:0,count3:2,borrower:"Zach Juneau", name: 'Zach Juneau',owner: 'Seth Taylor', status:"4 owned"},

];

const archive:IPLData[]=[
{id:16,count1:10,loan_status:2,count2:0,count3:2,borrower:"Eadwiella", name: 'Zach Juneau',owner: 'Seth Taylor', status:"10 owned"},
];


@Component({
    selector: 'dialog-overview-example-dialog',
    templateUrl: 'edit-flow.component.html',
  })
  export class DialogOverviewExampleDialog {
    details: DialogData;

    
    constructor(
        @Optional()   public dialogRef: MatDialogRef<DialogOverviewExampleDialog>,
      @Optional()  @Inject(MAT_DIALOG_DATA) public data: DialogData) {
   
this.details=data;
    
      }
  
    onNoClick(): void {
     
    }

    edit_loan(details){
        this.dialogRef.close({event:'Cancel',data:details});
    }
  
  }
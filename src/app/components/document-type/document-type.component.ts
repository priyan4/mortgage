import { Component, OnInit, ViewChild, Inject, Optional } from '@angular/core';
import {MatPaginator, MatSort, MatTableDataSource, MatDialogRef, MAT_DIALOG_DATA, MatDialog} from '@angular/material';
export interface DialogData {
  data: any;
 
}
@Component({
  selector: 'app-document-type',
  templateUrl: './document-type.component.html',
  styleUrls: ['./document-type.component.scss']
})
export class DocumentTypeComponent implements OnInit {
  displayedColumns: string[] = ['name','action'];
  dataSource = new MatTableDataSource<IPLData>(active);
  @ViewChild(MatPaginator, {static:true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static:true}) sort: MatSort;
constructor(public dialog:MatDialog) { }

ngOnInit() {
  this.dataSource.sort = this.sort;
  this.dataSource.paginator = this.paginator;
  console.log(active)
}
applyFilter(filterValue: string) {
this.dataSource.filter = filterValue.trim().toLowerCase();
}


delete_Loan(id){
    var index = active.findIndex(x => x.id === id);
    active.splice(index,1);
    this.dataSource=new MatTableDataSource(active);
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
 
}



}
export interface IPLData {
name: string;
id:number;
}


const active:IPLData[]=[
{ id:1,name: 'Ken Bunch'},
{ id:2,name: 'fg Bunch'},
{ id:3,name: 'hg Bunch'},
{ id:4,name: 'bn Bunch'},
{ id:5,name: 'fassg Bunch'},
{ id:6,name: 'Kvhnen Bunch'},

];


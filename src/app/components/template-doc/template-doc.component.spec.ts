import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TemplateDocComponent } from './template-doc.component';

describe('TemplateDocComponent', () => {
  let component: TemplateDocComponent;
  let fixture: ComponentFixture<TemplateDocComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TemplateDocComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TemplateDocComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

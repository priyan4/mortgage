import { Component, OnInit, ViewChild, Optional, Inject } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort, MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { AngularEditorConfig } from '@kolkov/angular-editor/lib/config';


export interface DialogData {
  data: any;
}
@Component({
  selector: 'app-template-doc',
  templateUrl: './template-doc.component.html',
  styleUrls: ['./template-doc.component.scss']
})
export class TemplateDocComponent implements OnInit {
  displayedColumns: string[] = ['name','description','type','category','action'];
  dataSource = new MatTableDataSource<IPLData>(active);
  @ViewChild(MatPaginator, {static:true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static:true}) sort: MatSort;
  loan_filter:any='1';
  show_error:boolean=false;

  constructor(public dialog:MatDialog) { }

  ngOnInit() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
}

  addNew(){
    const dialogRef = this.dialog.open(AddNewDialog, {
         
      data: ''
    });
    dialogRef.afterClosed().subscribe(result => {
      this.dataSource = new MatTableDataSource<IPLData>(active);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    });
  }

}



export interface IPLData {
  name:string;
  description:string;
  type:string;
  category:string;
  id:number;
}


const active:IPLData[]=[
  {id:1,name:"1003-Loan Application",description:"Please complete all fields",type:"TenOThree",category:"Document Requested"},
  {id:1,name:"1003-Loan Application",description:"Please complete all fields",type:"Regular",category:"Document Requested"},
  {id:1,name:"1003-Loan Application",description:"Please complete all fields",type:"Regular",category:"Document Requested"},
  {id:1,name:"1003-Loan Application",description:"Please complete all fields",type:"Regular",category:"Document Requested"},
  {id:1,name:"1003-Loan Application",description:"Please complete all fields",type:"Regular",category:"Document Requested"}

];



@Component({
  selector: 'dialog-overview-example-dialog',
  templateUrl: 'addNew.component.html',
  styleUrls: ['./template-doc.component.scss']
})
export class AddNewDialog {
  details: DialogData;
  add_new:any='1';
  selectedFile: File
  favoriteSeason: string;
  htmlContent:any;
  editorConfig: AngularEditorConfig={
    editable: true,
    spellcheck: true,
    
    minHeight: '50',
    maxHeight: '100',
    width: '50',
    minWidth: '50',
    translate: 'yes',
    enableToolbar: true,
    showToolbar: true,
    toolbarHiddenButtons: [
     ['underline',
     'strikeThrough',
     'subscript',
     'superscript',
     'justifyLeft',
     'justifyCenter',
     'justifyRight',
     'justifyFull',
     'indent',
     'outdent',
     'insertUnorderedList',
     'insertOrderedList',
     'heading',
     'fontName'],
      [ 'fontSize',
      'textColor',
      'backgroundColor',
      'customClasses',
      'link',
      'unlink',
      'insertImage',
      'insertVideo',
      'insertHorizontalRule',
      'removeFormat',
      'toggleEditorMode']
    ]
  }
  seasons: string[] = [' Borrower will respond by uploading a document', ' Acknowledgement: Borrower will simply acknowledge receipt'];
  constructor(
      @Optional()   public dialogRef: MatDialogRef<AddNewDialog>,
      @Optional()  @Inject(MAT_DIALOG_DATA) public data: DialogData) {
 
this.details=data;

  
    }

  onNoClick(): void {
   
  }

  onFileChanged(event){
    this.selectedFile = event.target.files[0]
  }

  // edit_loan(details){
  //     this.dialogRef.close({event:'Cancel',data:details});
  // }

}
